<?php

namespace App\Observers;

use App\Models\Challenge1\Product;

class ProductObserver
{

    public $afterCommit = true;
    
    /**
     * Handle the Product "created" event.
     *
     * @param  \App\Models\Challenge1\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        $invoice = $product->invoice;
        $invoice->total += 1;
        $invoice->save(); 
    }

    /**
     * Handle the Product "updated" event.
     *
     * @param  \App\Models\Challenge1\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        //
    }

    /**
     * Handle the Product "deleted" event.
     *
     * @param  \App\Models\Challenge1\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        //
    }

    /**
     * Handle the Product "restored" event.
     *
     * @param  \App\Models\Challenge1\Product  $product
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the Product "force deleted" event.
     *
     * @param  \App\Models\Challenge1\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        //
    }
}
