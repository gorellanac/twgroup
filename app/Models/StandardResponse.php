<?php

namespace App\Models;

class StandardResponse  {

    public $success;
    public $title;
    public $message;
    public $data;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {
        $this->success = false;
        $this->title = 'An error has ocurred';
        $this->message = "Unfortunely, we can process your request :(";
        $this->data = [];
    }

}