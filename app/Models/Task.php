<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'name', 'description', 'max_execution_date', 'active'
    ];

    /**
     * Relations
     */
    public function user() { return $this->belongsTo(User::class, 'user_id'); }
    public function logs() { return $this->hasMany(Log::class, 'task_id'); }
}
