<?php

namespace App\Models\Challenge1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice_id', 'name', 'quantity', 'price'
    ];

    /**
     * Relations
     */
    public function invoice() { return $this->belongsTo(Invoice::class, 'invoice_id'); }


    /**
     * Scopes
     */
    public function scopeNamesFinalPriceGreaterThan1M($query)
    {
        return $query
                ->select('name')
                ->whereRaw('(quantity * price) > ?', [1000000])->get();
                
    }
}
