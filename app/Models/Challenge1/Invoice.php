<?php

namespace App\Models\Challenge1;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = [
        'date', 'user_id', 'seller_id', 'type'
    ];

    /**
     * Relations
     */
    public function user() { return $this->belongsTo(User::class, 'user_id'); }
    public function products() { return $this->hasMany(Product::class, 'invoice_id'); }


    /**
     * Scopes
     */
    public function scopeTotal($query, int $id)
    {
        return $query
                ->join('products', 'invoices.id', '=', 'products.invoice_id')
                ->where('invoices.id', '=', $id)
                ->groupBy('invoices.id')
                ->select('invoices.id', DB::raw('SUM(products.price) as total_invoice'))->first();
    }

    public static function scopeWithProductsGreaterThan100($query)
    {
        return $query
                ->join('products', 'invoices.id', '=', 'products.invoice_id')
                ->where('products.quantity', '>', 100)
                ->select('invoices.id')->get();
    }
}
