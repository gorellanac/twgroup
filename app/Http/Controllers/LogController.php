<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Models\Task;
use App\Models\Log as LogModel;
use App\Http\Requests\CreateLogRequest;
use App\Models\StandardResponse;
use App\Exceptions\ModelNotFoundException;
use App\Exceptions\ActionNotAllowedException;
use App\Mail\LogConfirmationMail;

class LogController extends Controller
{
    private $standard;

    public function __construct()
    {
        $this->standard = new StandardResponse;
    }

    /**
     * Show the form for creating a new resource.
     * 
     * @param   int $taskId
     *
     * @return \Illuminate\Http\Response
     * 
     */
    public function create(int $taskId)
    {
        $user = Auth::user();

        try {

            $task = Task::find($taskId);
            throw_if(empty($task), new ModelNotFoundException);
            throw_if($task->user_id != $user->id, new ActionNotAllowedException);

            $data = [
                'task'      => $task,
                'addLog'    => true
            ];

            return view('logs.create', $data);

        } catch(ActionNotAllowedException $exception) {
            $this->standard->message = __('errors.action.not_allowed');
        } catch(ModelNotFoundException $exception) {
            $this->standard->message = __('errors.model.not_found');
        } catch(\Exception $exception) {
            Log::error($exception->getMessage());
        }

        return redirect()->route('tasks.index')->with('response', $this->standard);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param   CreateLogRequest    $request
     * @param   int                 $taskId
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLogRequest $request, int $taskId)
    {
        $user = Auth::user();
        
        try {

            $task = Task::find($taskId);
            throw_if(empty($task), new ModelNotFoundException);
            throw_if($task->user_id != $user->id, new ActionNotAllowedException);

            // Create log
            $data = $request->validated();
         
            $data['task_id'] = $taskId;
            $log = LogModel::create($data);
       
            // Send email
            // TO-DO: Should be queueble
            Mail::to($user->email)->send(new LogConfirmationMail($task, $log));
            
            $this->standard->success = true;
            $this->standard->message = __('custom.action.ok', ['obj' => Str::title(trans_choice('custom.log', 1)), 'action' => 'created']);

            return redirect()->route('tasks.index')->with('response', $this->standard);

        } catch(ActionNotAllowedException $exception) {
            $this->standard->message = __('errors.action.not_allowed');
        } catch(ModelNotFoundException $exception) {
            $this->standard->message = __('errors.model.not_found');
        } catch(\Exception $exception) {
            Log::error($exception->getMessage());
        }
        
        return back()->withInput()->with('response', $this->standard);
    }
}
