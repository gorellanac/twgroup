<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use App\Exceptions\ModelNotFoundException;
use App\Models\Challenge1\Product;
use App\Services\Challenge1Service;

class Challenge1 extends Controller
{

    private $service;

    public function __construct(Challenge1Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get total invoice
     * 
     */
    public function getTotalInvoice(int $invoiceId = 0)
    {
        $message = '';
        $success = false;
        $data = [];

        try {

            $data = $this->service->totalInvoice($invoiceId);
            $success = true;

        } catch(ModelNotFoundException $exception) {
            $message = "La factura {$invoiceId} no existe en el sistema";
        } catch(\Exception $e) {
            $message = $e->getMessage();
            Log::error($message);
        }   

        return response()->json([
            'success'   => $success,
            'message'   => $message,
            'data'      => $data
        ]);
    }

    public function getIdsInvoiceGreater100()
    {
        $message = '';
        $success = false;
        $data = [];

        try {

            $data = $this->service->invoiceIdsByWithProductsGreaterThan100();
            $success = true;

        } catch(\Exception $e) {
            $message = $e->getMessage();
            Log::error($message);
        }   

        return response()->json([
            'success'   => $success,
            'message'   => $message,
            'data'      => $data
        ]);
    }

    public function getNamesProductsTPGreaterThan1M()
    {
        $message = '';
        $success = false;
        $data = [];

        try {
            
            $data = $this->service->namesProductsTPGreaterThan1M();
            $success = true;

        } catch(\Exception $e) {
            $message = $e->getMessage();
            Log::error($message);
        }   

        return response()->json([
            'success'   => $success,
            'message'   => $message,
            'data'      => $data
        ]);
    }
}
