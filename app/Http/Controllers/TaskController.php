<?php

namespace App\Http\Controllers;

use App\Exceptions\ActionNotAllowedException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Models\Task;
use App\Exceptions\ModelNotFoundException;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\EditTaskRequest;
use App\Models\StandardResponse;

class TaskController extends Controller
{

    private $standard;

    public function __construct()
    {
        $this->standard = new StandardResponse;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        // Handle tasks to show them
        $tasks = Task::all()->map(function($item) use($user) {
            $item->blocked = ($item->user_id != $user->id);
            return $item;
        });

        $data = [
            'tasks' => $tasks
        ];

        return view('tasks.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        try {

            $task =  Task::find($id);
            throw_if(empty($task), new ModelNotFoundException);
            throw_if($task->user_id != $user->id, new ActionNotAllowedException);

            $data = [
                'task'      => $task,
                'showing'   => true,
                'addLog'    => true
            ];

            return view('tasks.show', $data);

        } catch(ActionNotAllowedException $exception) {
            $this->standard->message = __('errors.action.not_allowed');
        } catch(ModelNotFoundException $exception) {
            $this->standard->message = __('errors.model.not_found');
        } catch(\Exception $exception) {
            Log::error($exception->getMessage());
        }

        return redirect()->route('tasks.index')->with('response', $this->standard);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = Auth::user();

        try {

            $task =  Task::find($id);
            throw_if(empty($task), new ModelNotFoundException);
            throw_if($task->user_id != $user->id, new ActionNotAllowedException);

            $data = [
                'task'      => $task,
                'addLog'    => true
            ];

            return view('tasks.edit', $data);

        } catch(ActionNotAllowedException $exception) {
            $this->standard->message = __('errors.action.not_allowed');
        } catch(ModelNotFoundException $exception) {
            $this->standard->message = __('errors.model.not_found');
        } catch(\Exception $exception) {
            Log::error($exception->getMessage());
        }

        return redirect()->route('tasks.index')->with('response', $this->standard);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTaskRequest $request)
    {
        $user = Auth::user();
    
        try {

            $data = $request->validated();
            $data['user_id'] = $user->id;

            // Create new task
            $task = Task::create($data);

            $this->standard->success = true;
            $this->standard->message = __('custom.action.ok', ['obj' => Str::title(trans_choice('custom.task', 1)), 'action' => 'created']);

            return redirect()->route('tasks.index')->with('response', $this->standard);

        } catch(\Exception $exception) {
            Log::error($exception->getMessage());
        }
        
        return back()->withInput()->with('response', $this->standard);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditTaskRequest $request, $id)
    {
        $user = Auth::user();
    
        try {

            $task = Task::find($id);
            throw_if(empty($task), new ModelNotFoundException);
            throw_if($task->user_id != $user->id, new ActionNotAllowedException);

            // Update task
            $task->update($request->validated());

            $this->standard->success = true;
            $this->standard->message = __('custom.action.ok', ['obj' => Str::title(trans_choice('custom.task', 1)), 'action' => 'updated']);

            return redirect()->route('tasks.index')->with('response', $this->standard);

        } catch(ActionNotAllowedException $exception) {
            $this->standard->message = __('errors.action.not_allowed');
        } catch(ModelNotFoundException $exception) {
            $this->standard->message = __('errors.model.not_found');
        } catch(\Exception $exception) {
            Log::error($exception->getMessage());
        }
        
        return back()->withInput()->with('response', $this->standard);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
    
        try {

            $task = Task::find($id);
            throw_if(empty($task), new ModelNotFoundException);
            throw_if($task->user_id != $user->id, new ActionNotAllowedException);

            // Delete task
            $task->delete();

            $this->standard->success = true;
            $this->standard->message = __('custom.action.ok', ['obj' => Str::title(trans_choice('custom.task', 1)), 'action' => 'deleted']);

            return redirect()->route('tasks.index')->with('response', $this->standard);

        } catch(ActionNotAllowedException $exception) {
            $this->standard->message = __('errors.action.not_allowed');
        } catch(ModelNotFoundException $exception) {
            $this->standard->message = __('errors.model.not_found');
        } catch(\Exception $exception) {
            Log::error($exception->getMessage());
        }
        
        return back()->withInput()->with('response', $this->standard);
    }
}
