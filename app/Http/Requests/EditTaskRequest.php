<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class EditTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|string|max:256',
            'description'           => 'required|string|max:512',
            'max_execution_date'    => 'required|date|date_format:Y-m-d|after_or_equal:'.Carbon::now()->format('Y-m-d'),
        ];
    }
}
