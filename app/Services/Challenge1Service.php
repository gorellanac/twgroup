<?php

namespace App\Services;

use App\Exceptions\ModelNotFound;
use App\Models\Challenge1\Invoice;
use App\Models\Challenge1\Product;

class Challenge1Service {

    /**
     * Get total invoice
     * 
     * @param   int $invoiceId  Invoice ID to calculate total
     * 
     * @return  array
     */
    public function totalInvoice(int $invoiceId) : array
    {
        // Check if invoice exists
        $invoice = Invoice::find($invoiceId);
        throw_if(empty($invoice), new ModelNotFound);

        $total = 0;
        foreach($invoice->products as $product) {
            $total += $product->price;
        }

        return [
            'usingModel'    => Invoice::total($invoiceId)['total_invoice'],
            'usingEloquent' => $total
        ];
    }

    /**
     * All invoices with products quantity > 100
     */
    public function invoiceIdsByWithProductsGreaterThan100()
    {
        $invoices = Invoice::all()->reject(function($item) {
            $reject = true;
            foreach($item->products as $product) {
                if($product->quantity > 100) {
                    $reject = false;
                }
            }
            return $reject;
        })->pluck('id')->toArray();

        return [
            'usingModel'    => Invoice::withProductsGreaterThan100(),
            'usingEloquent' => $invoices
        ];
    }

    /**
     * Product names with final price greater than 1M
     */
    public function namesProductsTPGreaterThan1M()
    {
        $products = Product::all()->filter(function($item) {
            return ($item->price * $item->quantity) > 1000000;
        })->pluck('name')->toArray();

        return [
            'usingModel'    => Product::namesFinalPriceGreaterThan1M(),
            'usingEloquent' => $products
        ];
    }

}
