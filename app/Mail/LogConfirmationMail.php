<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Task;
use App\Models\Log;

class LogConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Task
     *
     * @var Task
     */
    public $task;
    /**
     * Log
     *
     * @var Log
     */
    public $log;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Task $task, Log $log)
    {
        $this->task = $task;
        $this->log = $log;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS_NOTIFICATIONS'), env('MAIL_FROM_NAME'))
                    ->subject('New log on task!')
                    ->view('mails.logs.created');
    }
}
