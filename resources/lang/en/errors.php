<?php

return [

/*
|--------------------------------------------------------------------------
| Custom error messages
|--------------------------------------------------------------------------
|
*/

'model.not_found' => "The task doesn't exist in the system!",
'action.not_allowed' => "You are not authorized to do this action!",

];