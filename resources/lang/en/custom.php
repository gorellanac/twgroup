<?php

return [

/*
|--------------------------------------------------------------------------
| Custom app messages
|--------------------------------------------------------------------------
|
*/

'task'      => 'task|tasks',
'log'       => 'log|logs',

'action.ok' => ":obj has been :action successfully! :)",

];