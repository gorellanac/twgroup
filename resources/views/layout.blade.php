<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">@yield('title')</h2>
    </x-slot>
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="bg-white px-3 py-3 rounded-top">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session('response'))
                    <div class="alert alert-{{ session('response')->success ? 'success' : 'danger' }}">{{ session('response')->message }}</div>
                @endif
                @yield('content')
            </div>
        </div>
    </div>
</x-app-layout>