@extends('layout')
@section('title', 'Create new log for task '.$task->id)
@section('content')
<form method="post" action="{{ route('logs.store', $task->id) }}">
    @csrf
    <div class="form-group my-3">
        <label for="comment">Comments</label>
        <textarea class="form-control" name="comment" id="comment" rows="3" placeholder="Log comments" required>{{ old('comment') ?? '' }}</textarea>
    </div>
    <button type="submit" class="btn btn-primary text-white">Add log</button>
    <a href="{{ route('tasks.index') }}" role="button" class="btn btn-light">Tasks list</a>
    <a href="{{ route('tasks.show', $task->id) }}" role="button" class="btn btn-light">Back to task</a>
</form>
@endsection