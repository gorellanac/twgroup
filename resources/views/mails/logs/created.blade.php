<!DOCTYPE html>
<html>
<head>
    <title>A new log has been created</title>
    <style>
        body.custom {
            font-family: Arial, Helvetica, sans-serif;
            color: #000;
            margin:0;
            background-color: #ededed;
        }
        a.custom {
            text-decoration: none;
            color:#0A15A0;
        }
        a.custom:hover {
            color:#2130EE;
        }
        a.custom:active {
            color:#2130EE;
        }
        a.btn {
            padding:15px 25px;background:#AF10DD;color:#fff; border-radius: 5px;text-decoration:none;
        }
        a.btn:visited { color:#fff; }
        a.btn:hover { color:#fff; }
        a.btn:active { color:#fff; }
    </style>
</head>
<body class="custom">
    <div style="background-color:#ededed">
        <center>
            <table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" style="width:100%;max-width:600px;display:block">
                <tbody>
                    <tr>
                        <td><div style="min-width:600px"></div></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width:100%;background-color:#fff;margin-top:30px;margin-bottom:30px;">
                                <tbody>
                                    <tr>
                                        <td valign="top">
                                            <!-- content -->
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="min-width:100%;padding:20px">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top">
                                                            <h3>Hello {{ $task->user->name }},</h3>
                                                            <p style="margin: 20px auto;">
                                                                A new log has been created for your task #{{ $task->id }} {{ $task->name }}
                                                            </p>
                                                            <p style="margin: 20px auto;">
                                                                Comments: <strong>{{ $log->comment }}</strong>.
                                                            </p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- end content -->
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </center>
    </div>
</body>
</html>