@extends('layout')
@section('title', 'Edit task')
@section('content')
<form method="post" action="{{ route('tasks.update', $task->id) }}">
    @csrf
    @method('PUT')
    @include('tasks._form', [
        'submitText' => 'Edit task'
    ])
</form>
@endsection