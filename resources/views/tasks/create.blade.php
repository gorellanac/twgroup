@extends('layout')
@section('title', 'Create new task')
@section('content')
<form method="post" action="{{ route('tasks.store') }}">
    @csrf
    @include('tasks._form', [
        'submitText' => 'Create task'
    ])
</form>
@endsection