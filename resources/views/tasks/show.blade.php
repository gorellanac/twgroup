@extends('layout')
@section('title', 'Show task details')
@section('content')
@include('tasks._form', [
    'submitText'    => 'Show task'
])
@endsection