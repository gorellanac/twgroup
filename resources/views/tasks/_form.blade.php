
@isset($addLog)
<a href="{{ route('logs.create', $task->id) }}" class="btn btn-success text-white" role="button">+ Add log to task</a>
@endisset
<div class="form-group my-3">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="name" id="name" value="{{ old('name') ?? $task->name ?? '' }}" placeholder="Task name" required @if(isset($showing)) disabled @endif />
</div>
<div class="form-group my-3">
    <label for="name">Maximum execution date</label>
    <input type="text" class="form-control" name="max_execution_date" id="max_execution_date" value="{{ old('max_execution_date') ?? $task->max_execution_date ?? '' }}" @if(isset($showing)) disabled @endif placeholder="Maximum execution date, format: yyyy-mm-dd" required />
</div>
<div class="form-group my-3">
    <label for="description">Description</label>
    <textarea class="form-control" name="description" id="description" rows="3" placeholder="Write a short description for your task" @if(isset($showing)) disabled @endif required>{{ old('description') ?? $task->description ?? '' }}</textarea>
</div>
@if(!isset($showing))
<button type="submit" class="btn btn-primary text-white">{{ $submitText }}</button>
@endif
<a href="{{ route('tasks.index') }}" role="button" class="btn btn-light">Cancel</a>