@extends('layout')
@section('title', 'Tasks list')
@section('content')
<a href="{{ route('tasks.create') }}" role="button" class="btn btn-success btn-lg text-white">+ Add task</a>
<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Owner</th>
            <th scope="col">Execution date</th>
            <th scope="col">Description</th>
            <th scope="col">Logs number</th>
            <th scope="col">Actions</th>
        </tr>
  </thead>
  <tbody>
        @forelse($tasks as $task)
        <tr>
            <th scope="row">{{ $task->id }}</th>
            <td>{{ $task->name }}</td>
            <td>{{ $task->user->name }} ({{ $task->user->email }})</td>
            <td>{{ $task->max_execution_date }}</td>
            <td>{{ $task->description }}</td>
            <td>{{ $task->logs->count() }}</td>
            <td style="width:20%">
                @if(!$task->blocked)
                <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-primary btn-sm text-white" role="button">Show</a>
                <a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-info btn-sm text-white" role="button">Edit</a>
                <form id="delete-task-{{ $task->id }}" style="display: inline-block !important" action="{{ route('tasks.destroy', $task->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-sm text-white inline-block do-delete" type="button" data-id="{{ $task->id }}">Delete</button>
                </form>
                @endif
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="7">No tasks found!</td>
        </tr>
        @endforelse
  </tbody>
</table>
@endsection
@push('scripts')
<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script>
    $(document).ready(function(e) {
        $(document).on('click', '.do-delete', function(evt) {
            const $id = $(this).data('id');
            if(confirm("Are you sure you want to delete the selected task?")){
                $('#delete-task-'+$id).submit();
            } else {
                return false;
            }
        });
    });
</script>
@endpush