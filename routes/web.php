<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\LogController;

require_once 'challenge1.php';

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth:sanctum', 'verified']], function() {
    Route::resource('/tasks', TaskController::class);
    Route::get('/logs/task/{taskId}/create', [LogController::class, 'create'])->name('logs.create');
    Route::post('/logs/task/{taskId}', [LogController::class, 'store'])->name('logs.store');
});
