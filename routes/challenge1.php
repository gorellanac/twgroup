<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Challenge1;

Route::get('/challenge1/invoice/total/{invoiceId?}', [Challenge1::class, 'getTotalInvoice'])->name('challenge1.invoice.total');
Route::get('/challenge1/invoice/ids_greater_100', [Challenge1::class, 'getIdsInvoiceGreater100'])->name('challenge1.invoice.ids_greater_100');
Route::get('/challenge1/product/names_final_price_greater_1m', [Challenge1::class, 'getNamesProductsTPGreaterThan1M'])->name('challenge1.invoice.ids');